// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: "AIzaSyDoQJFYxnoiTXRryeJWMI1YkJWOGEovfuA",
  authDomain: "firedaw-2ddff.firebaseapp.com",
  databaseURL: "https://firedaw-2ddff.firebaseio.com",
  projectId: "firedaw-2ddff",
  storageBucket: "firedaw-2ddff.appspot.com",
  messagingSenderId: "799603547850",
  appId: "1:799603547850:web:95151770e2d90eb4df9e33"
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
