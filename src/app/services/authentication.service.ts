import { Injectable } from '@angular/core';
import {AngularFireAuth} from "@angular/fire/auth";
import {AngularFirestore} from '@angular/fire/firestore';
import { user} from '../model/userinterface';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  usuarios:user;

  constructor(private afAuth:AngularFireAuth, private db:AngularFirestore) { }

  async signup(mail:string, pass:string){
    return this.afAuth.auth.signInWithEmailAndPassword(mail,pass)
    .then((newCredential:firebase.auth.UserCredential) => {
      console.log(newCredential);
    })
    .catch(error => {
      console.log(error);
      throw new Error (error);
    });
  }

  async createUser(mail:string, pass:string){
    return this.afAuth.auth.createUserWithEmailAndPassword(mail,pass)
    .then((newCredential:firebase.auth.UserCredential) => {
      console.log(newCredential);
        this.usuarios.mail=mail;
        this.usuarios.pass=pass;
        this.db.doc('/user/${user.id}').set({ user:"" });

    })
    .catch(error => {
      console.log(error);
      throw new Error (error);
    });
  }
}
