import { AngularFireAuthModule } from '@angular/fire/auth';
import { AuthenticationService } from './../services/authentication.service';
import { Router } from '@angular/router';
import { Component } from '@angular/core';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  authService: any;
  newUser: any;
  toastService: any;

  constructor(private router:Router) {
   
  }
  disableInput:boolean=false;
  textForgot:string = "forgot your password"
  textUsername:string = "";
  textPass:string = "";
  colore:string="";
  colore2:string="";
  
 onClickforgot(){
   this.textForgot= "te jodes";
 }
 onClickboton(){
  if((this.textUsername =="") || (this.textPass =="")){
   if(this.textUsername==""){
    this.colore="danger";
   }else{
    this.colore="";

   }
   if(this.textPass==""){
    this.colore2="danger";
   }else{
    this.colore2="";

   }
  }else{
    this.router.navigateByUrl('wellcome');
   }

 }

 chekuser(){
  if(this.textUsername==""){
    this.colore="danger";
   }else{
    this.colore="";

   }
  }
  chekuser2(){

   if(this.textPass==""){
    this.colore2="danger";
   }else{
    this.colore2="";

   }
 }
 registrarse(){
  this.router.navigateByUrl('registro');

 }
 

}
