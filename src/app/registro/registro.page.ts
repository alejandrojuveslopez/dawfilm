import { AngularFirestore } from '@angular/fire/firestore';
import { AuthenticationService } from './../services/authentication.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { user} from '../model/userinterface';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {
  usuarios:user;

  constructor(private router:Router, private authService:AuthenticationService, private db:AngularFirestore) {
   this.mostrar=true;
  }
  disableInput:boolean=false;
  textForgot:string = "forgot your password"
  textUsername:string = "";
  textPass:string = "";
  colore:string="";
  colore2:string="";
  mostrar:boolean;
 onClickforgot(){
   this.textForgot= "te jodes";
 }
 onClickboton(){
   
  if((this.textUsername =="") || (this.textPass =="")){
    if(this.textUsername==""){
     this.colore="danger";
    }else{
     this.colore="";
 
    }
    if(this.textPass==""){
     this.colore2="danger";
    }else{
     this.colore2="";
 
    }
   }else{
     this.authService.createUser(this.textUsername,this.textPass)
     .then(() => {
       console.log("Usuario creado correctamente");
     },error => {
       console.log(error);
     })
     /*this.router.navigateByUrl('home');*/
 
    }

 }
  /*async onClickSaveButton(){
    this.newUser.mail = this.textUsername;
    this.newUser.password = this.textPass;
    this.authService.signupUser(this.newUser).then(()=> {
    this.toastService.createToast('Usuario creado correctamente. Bienvenido', false);
   
 })
 }*/
 chekuser(){
  if(this.textUsername==""){
    this.colore="danger";
   }else{
    this.colore="";

   }
  }
  chekuser2(){

   if(this.textPass==""){
    this.colore2="danger";
   }else{
    this.colore2="";

   }
 }
 

  ngOnInit() {
  }

}
